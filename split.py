import numpy as np
import random
from datetime import datetime
import os
import sys
import geopandas as gpd

def writeSplit(idx, data, outFileName ):
	# fold = []
	# for i in idx:
	# 	fold.append( data[i] )
	# np.save(outFileName, np.array(fold) )
	fold = data[idx]
	np.save(outFileName, fold )

def getIdxSplit(hash_class, perc_train, perc_valid):
	train_id = []
	valid_id = []
	test_id = []
	
	dt = datetime.now()
	random.seed( dt.microsecond )
	classes = hash_class.keys()
	
	for cl in classes:
		cl_train_id = []
		cl_valid_id = []
		cl_test_id = []

		ids = hash_class[cl]	
		random.shuffle(ids)
		random.shuffle(ids)
		thr = int(len(ids) * perc_train)
		thr_val = int(len(ids) * perc_valid)	

		for i in range(len(ids)):
			if i < thr:
				cl_train_id.append( ids[i] ) 
			elif i < (thr+thr_val):
				cl_valid_id.append( ids[i] ) 
			else:
				cl_test_id.append( ids[i] ) 
		
		cl_train_idx = np.where(np.isin(all_id, cl_train_id))
		cl_valid_idx = np.where(np.isin(all_id, cl_valid_id))
		cl_test_idx = np.where(np.isin(all_id, cl_test_id))

		print ("%d, %d, %d over %d" % (ds_label[cl_train_idx].shape[0], ds_label[cl_valid_idx].shape[0], ds_label[cl_test_idx].shape[0], class_count[cl]))
		
		train_id.extend(cl_train_id)
		valid_id.extend(cl_valid_id)
		test_id.extend(cl_test_id)

	print ("============")

	train_idx = np.where(np.isin(all_id, train_id))
	valid_idx = np.where(np.isin(all_id, valid_id))
	test_idx = np.where(np.isin(all_id, test_id))

	return train_idx, valid_idx, test_idx

	

path_in = sys.argv[1] #"data"
path_out = sys.argv[2] #"splits"
perc_train = float(sys.argv[3]) #0.5
perc_valid = float(sys.argv[4]) #0.2

if not os.path.exists(path_out):
	os.makedirs(path_out)

print (path_in)

ds_label = np.load( path_in+"/gt_id_labels.npy" )
gapfTimeSeries = np.load( path_in+"/gapf_ts_data.npy" )
ungapfTimeSeries = np.load( path_in+"/ungapf_ts_data.npy" )

hash_class = {}
classes = np.unique(ds_label[:,1])
for cl in classes :
	obj_id = ds_label[ds_label[:, 1] == cl][:,0]
	hash_class[cl] = list(np.unique(obj_id))

all_id = ds_label[:,0]
class_count = np.bincount(ds_label[:,1])

for split_id in range(10):
	print ("split_id %d" % split_id)
	
	train_idx, valid_idx, test_idx = getIdxSplit(hash_class, perc_train, perc_valid)

	if not os.path.exists(path_out+"/gapfTimeSeries/"):
		os.makedirs(path_out+"/gapfTimeSeries/")
	
	if not os.path.exists(path_out+"/ungapfTimeSeries/"):
		os.makedirs(path_out+"/ungapfTimeSeries/")

	if not os.path.exists(path_out+"/ground_truth/"):
		os.makedirs(path_out+"/ground_truth/")
	
	
	#write Gapfilled time series splits
	outFileTrain = path_out+"/gapfTimeSeries/train_x"+str(split_id)+"_"+str(int(perc_train*100))+".npy"
	writeSplit(train_idx, gapfTimeSeries, outFileTrain )

	outFileTest = path_out+"/gapfTimeSeries/test_x"+str(split_id)+"_"+str(int(perc_train*100))+".npy"
	writeSplit(test_idx, gapfTimeSeries, outFileTest )
	
	outFileValid = path_out+"/gapfTimeSeries/valid_x"+str(split_id)+"_"+str(int(perc_train*100))+".npy"
	writeSplit(valid_idx, gapfTimeSeries, outFileValid )
	
	#write Raw (Ungapfilled) time series splits
	outFileTrain = path_out+"/ungapfTimeSeries/train_x"+str(split_id)+"_"+str(int(perc_train*100))+".npy"
	writeSplit(train_idx, ungapfTimeSeries, outFileTrain )

	outFileTest = path_out+"/ungapfTimeSeries/test_x"+str(split_id)+"_"+str(int(perc_train*100))+".npy"
	writeSplit(test_idx, ungapfTimeSeries, outFileTest )

	outFileValid = path_out+"/ungapfTimeSeries/valid_x"+str(split_id)+"_"+str(int(perc_train*100))+".npy"
	writeSplit(valid_idx, ungapfTimeSeries, outFileValid )

	#write ground_truth
	outFileTrain = path_out+"/ground_truth/train_y"+str(split_id)+"_"+str(int(perc_train*100))+".npy"
	np.save(outFileTrain, np.array( ds_label[train_idx] ))

	outFileTest = path_out+"/ground_truth/test_y"+str(split_id)+"_"+str(int(perc_train*100))+".npy"
	np.save(outFileTest, np.array( ds_label[test_idx] ))

	outFileValid = path_out+"/ground_truth/valid_y"+str(split_id)+"_"+str(int(perc_train*100))+".npy"
	np.save(outFileValid, np.array( ds_label[valid_idx] ))



