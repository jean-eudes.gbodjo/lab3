http://ataspinar.com/2018/07/05/building-recurrent-neural-networks-in-tensorflow/
https://medium.com/machine-learning-algorithms/build-basic-rnn-cell-with-static-rnn-707f41d31ee1
http://monik.in/a-noobs-guide-to-implementing-rnn-lstm-using-tensorflow/

python prepare_data.py
python split.py data splits 0.5 0.2

bash launch_rf_gapf.sh
bash restore_rf_gapf.sh
python compute_stats.py splits/ground_truth rf_gapf/results 50

bash launch_rnn_gapf.sh
bash restore_rnn_gapf.sh
python compute_stats.py splits/ground_truth rnn_gapf/results 50
