#!/bin/bash

dir="."
path_ts_gapf="splits/gapfTimeSeries"
path_gt="splits/ground_truth"
pt=50

for i in {0..9}
do
    echo $i
    python rf_gapf.py $path_ts_gapf/train_x$i\_$pt.npy $path_gt/train_y$i\_$pt.npy $path_ts_gapf/valid_x$i\_$pt.npy $path_gt/valid_y$i\_$pt.npy $i rf_gapf
done


