import glob, os
from pprint import pprint
import rasterio
import numpy as np
from sklearn.preprocessing import MinMaxScaler
import gdal, ogr
import geopandas as gpd
import random

def create_time_series (inPath, gt, outPath, bands=["B2","B3","B4","B8","GAPF_MASK"]) :
    
    # Gapfilled Time Series
    
    if not os.path.isfile(os.path.join(outPath,"gapf_ts_data.npy")) : 
        lstFile = glob.glob(inPath+os.sep+"GAPF"+os.sep+"B*_GAPF.tif")
        lstFile.sort()
        lstFile.extend(glob.glob(inPath+os.sep+"INDICES"+os.sep+"NDVI*_GAPF.tif"))
        # pprint (lstFile)

        with rasterio.open(gt) as gt_ds :
            gt_array = gt_ds.read(1)
        
        gt_indices = np.nonzero(gt_array)
        # print (gt_indices)

        gapf_ts = None
        
        for File in lstFile :
            band_min = 100000000
            band_max = -100000000
            gapf_band = None
            with rasterio.open(File) as ds :
                for c in range(ds.count):
                    gapf_read = ds.read(c+1).astype(float)

                    if np.min(gapf_read) < band_min :
                        band_min = np.min(gapf_read)
                    
                    if np.max(gapf_read) > band_max :
                        band_max = np.max(gapf_read)

                    if gapf_band is None :
                        gapf_band = gapf_read[gt_indices]
                    else :
                        gapf_band = np.column_stack((gapf_band,gapf_read[gt_indices]))
            print (band_min, band_max)

            if gapf_ts is None :
                gapf_ts =  (gapf_band - band_min) / (band_max - band_min)
            else :
                gapf_ts = np.column_stack((gapf_ts,((gapf_band - band_min) / (band_max - band_min))))
        
        print (gapf_band.shape)
        print (gapf_ts.shape)

        n_pixels, n_timestamp = gapf_band.shape[0], gapf_band.shape[1]
        n_bands = len(lstFile)
 
        gapf_ts_data = None
        for i in range(n_pixels): # Pixels
            gapf_ts_array = None
            for j in range(n_timestamp): # Time stamps
                lst = []
                for c in range(n_bands) : # bands + indices number
                    lst.append(gapf_ts[i,j+c*n_timestamp])
                # print (lst)
                if gapf_ts_array is None :
                    gapf_ts_array = np.array(lst,dtype=float)
                else :
                    gapf_ts_array = np.hstack((gapf_ts_array,np.array(lst,dtype=float)))
            if gapf_ts_data is None :
                gapf_ts_data = gapf_ts_array
            else :
                gapf_ts_data = np.vstack((gapf_ts_data,gapf_ts_array))
        
        print (gapf_ts_data.shape)
        np.save(os.path.join(outPath,"gapf_ts_data.npy"),gapf_ts_data)
    
    # UnGapfilled Time Series
    if not os.path.isfile(os.path.join(outPath,"ungapf_ts_data.npy")) : 
        
        # lstFile = glob.glob(inPath+os.sep+"GAPF"+os.sep+"B*_S2.tif")
        # lstFile.sort()
        lstFile = glob.glob(inPath+os.sep+"GAPF"+os.sep+"B*_GAPF.tif")
        lstFile.sort()
        lstFile.extend(glob.glob(inPath+os.sep+"INDICES"+os.sep+"NDVI*_GAPF.tif"))
        # pprint (lstFile)

        gapf_mask = glob.glob(inPath+os.sep+"GAPF"+os.sep+"GAPF_MASK*_S2.tif")[0]
        # print (gapf_mask)

        with rasterio.open(gt) as gt_ds :
            gt_array = gt_ds.read(1)
        gt_indices = np.nonzero(gt_array)
        # print (gt_indices)

        # nir_file = glob.glob(inPath+os.sep+"GAPF"+os.sep+"B8*_S2.tif")[0]
        # red_file = glob.glob(inPath+os.sep+"GAPF"+os.sep+"B4*_S2.tif")[0]


        ungapf_ts = None
        # Bands 
        for File in lstFile :
            band_min = 100000000
            band_max = -100000000
            ungapf_band = None

            with rasterio.open(File) as ds :
                for c in range(ds.count):
                    ungapf_read = ds.read(c+1).astype(float)

                    with rasterio.open(gapf_mask) as mask_ds :
                        mask = mask_ds.read(c+1)

                    ungapf_read = np.where(mask==1,np.nan,ungapf_read)
                    
                    if np.nanmin(ungapf_read) < band_min :
                        band_min = np.nanmin(ungapf_read)
                    
                    if np.nanmax(ungapf_read) > band_max :
                        band_max = np.nanmax(ungapf_read)

                    if ungapf_band is None :
                        ungapf_band = ungapf_read[gt_indices]
                    else :
                        ungapf_band = np.column_stack((ungapf_band,ungapf_read[gt_indices]))
                        
            print (band_min, band_max)

            if ungapf_ts is None :
                ungapf_ts =  (ungapf_band - band_min) / (band_max - band_min)
            else :
                ungapf_ts = np.column_stack((ungapf_ts,((ungapf_band - band_min) / (band_max - band_min))))
        
        print (ungapf_band.shape)
        print (ungapf_ts.shape)

        n_pixels, n_timestamp = ungapf_band.shape[0], ungapf_band.shape[1]
        n_bands = len(lstFile)
        
        # # NDVI 
        # ndvi _array = None
        # ndvi_min = 100000000
        # ndvi_max = -100000000
        # for i in range(n_timestamp):
        #     with rasterio.open(nir_file) as ds:
        #         nir = ds.read(i+1)[gt_indices]
        #     with rasterio.open(red_file) as ds:
        #         red = ds.read(i+1)[gt_indices]
        #     with rasterio.open(gapf_mask) as mask_ds :
        #         mask = mask_ds.read(i+1)[gt_indices]
            
        #     if ndvi_array is None :
        #         ndvi_array = np.where(mask==1,np.nan,(nir-red)/(nir+red))
        #     else :
        #         ndvi_array = np.column_stack((ndvi_array,np.where(mask==1,np.nan,(nir-red)/(nir+red))))
 
        ungapf_ts_data = None
        for i in range(n_pixels): # Pixels
            ungapf_ts_array = None
            for j in range(n_timestamp): # Time stamps
                lst = []
                for c in range(n_bands) : # bands + indices number
                    lst.append(ungapf_ts[i,j+c*n_timestamp])
                # print (lst)
                if ungapf_ts_array is None :
                    ungapf_ts_array = np.array(lst,dtype=float)
                else :
                    ungapf_ts_array = np.hstack((ungapf_ts_array,np.array(lst,dtype=float)))
            if ungapf_ts_data is None :
                ungapf_ts_data = ungapf_ts_array
            else :
                ungapf_ts_data = np.vstack((ungapf_ts_data,ungapf_ts_array))
        
        print (ungapf_ts_data.shape)
        np.save(os.path.join(outPath,"ungapf_ts_data.npy"),ungapf_ts_data)


def prepare_gt (inPath, inShp, outPath) :

    random.seed(8896)
    
    df = gpd.read_file(inShp)
    df['surface'] = df.area
    # print(df)

    lstClass = df["Code"].unique()
    lstClass.sort()

    # nb_obj = [64,40,200,260,126,18,16,15,10,13,660]
    # nb_obj = [50, 37, 290, 260, 128, 50, 18, 248, 70, 153, 340]
    # nb_obj = [40, 37, 290, 260, 128, 491, 450, 248, 70, 153, 340]

    # nb_obj = [31, 31, 173, 260, 75, 44, 9, 11, 19, 134, 140]

    # nb_obj = [31, 31, 173, 260, 75, 44, 62, 79, 21, 125, 167]

    # nb_obj = []
    nb_m = [850,1000,850,1000,900,1500,3000,3500,15000,13000,5000]

    lstFile = glob.glob(inPath+os.sep+"GAPF"+os.sep+"B*_GAPF.tif")
    ds = gdal.Open(lstFile[0])
    geoT = ds.GetGeoTransform()
    proj = ds.GetProjection()
    xsize = ds.RasterXSize
    ysize = ds.RasterYSize
    ds = None

    outID = []
    
    for c in lstClass: 
        # obj = 1
        lst_surf = df["surface"][df["Code"]==c].tolist()
        m = np.median(lst_surf)
        # q1 = np.percentile(lst_surf,25)
        # q3 = np.percentile(lst_surf,75)
        # binf = np.abs(q1 - 1.5 * (q3-q1))
        # bsup = np.abs(q3 + 1.5 * (q3-q1))

        if c == 4 :
            lstID = df["ID"][(df["Code"]==c)].tolist()
        else :
            lstID = df["ID"][(df["Code"]==c) & (df["surface"] < m+nb_m[c-1]) & (df["surface"] > m-nb_m[c-1]) ].tolist()
        # elif c in [6,10] :
        #     bsup = np.abs(q3-q1)
        #     lstID = df["ID"][(df["Code"]==c) & (df["surface"] < bsup)].tolist()
        # elif c in [7,8,9] :
        #     bsup = np.abs(q3-q1)
        #     lstID = df["ID"][(df["Code"]==c) & (df["surface"] < bsup)].tolist()
        # elif c == 11 :
        #     binf = np.abs(q1 - 1.5 * (q3-q1))
        #     lstID = df["ID"][(df["Code"]==c) & (df["surface"] > binf)].tolist()
        #     del lstID[lstID.index(2)]
        # else : 
        #     binf = np.abs(q1 - 1.5 * (q3-q1))
        #     bsup = np.abs(q3 + 1.5 * (q3-q1))
        #     lstID = df["ID"][(df["Code"]==c) & (df["surface"] < bsup) & (df["surface"] > binf) ].tolist()
        
        # print (len(lstID),"/",len(df["ID"][(df["Code"]==c)].tolist()))
        
        random.shuffle(lstID)
        outID.extend(lstID)
        # outID.extend(lstID[:nb_obj[c-1]])
        #s = [0,0,0,0,0,0,0,0,0,0,0,0]
        #while s[c] <= 5000 and obj<=len(lstID):
        #outID.extend(lstID[:obj])
        # outID.extend(lstID)

    outdf = df[df["ID"].isin(outID)] #outID
    # print (outdf)
    gt = os.path.join(outPath,"GT_SAMPLES.shp")
    outdf.to_file(gt)

    mem_drv = gdal.GetDriverByName("GTiff")
    gt_shp = ogr.Open(gt)
    gt_layer = gt_shp.GetLayer()

    dest1 = mem_drv.Create(os.path.join(outPath,'GT_SAMPLES.tif'), xsize, ysize, 1, gdal.GDT_Byte)
    dest1.SetGeoTransform(geoT)
    dest1.SetProjection(proj)
    gdal.RasterizeLayer(dest1, [1], gt_layer, options=["ATTRIBUTE=Code"])
    gt_rst = dest1.GetRasterBand(1).ReadAsArray()

    print (np.bincount(gt_rst.reshape(xsize*ysize)))
        #s = np.bincount(gt_rst.reshape(xsize*ysize))
        #nb_obj.append(obj)
        #obj+=1
        
    #print (nb_obj)

    mem_drv = gdal.GetDriverByName("MEM")
    dest2 =  mem_drv.Create('', xsize, ysize, 1, gdal.GDT_UInt16)
    dest2.SetGeoTransform(geoT)
    dest2.SetProjection(proj)
    gdal.RasterizeLayer(dest2, [1], gt_layer, options=["ATTRIBUTE=ID"])
    ID_rst  = dest2.GetRasterBand(1).ReadAsArray()

    gt_shp = None
    gt_layer = None
    dest1 = None
    dest2 = None
    mem_drv = None

    gt_indices = np.nonzero(gt_rst)
    gt_labels = gt_rst[gt_indices]
    gt_ID = ID_rst[gt_indices]

    gt_id_labels = np.column_stack((gt_ID,gt_labels))
    np.save(os.path.join(outPath,"gt_id_labels.npy"),gt_id_labels)


if __name__ == "__main__":
    
    inPath = "/media/je/SATA_1/Lab1/REUNION/OUTPUT"
    outPath = "/media/je/SATA_1/Lab3/data"
    inShp = "/media/je/SATA_1/Lab1/REUNION/BD_GABIR_2017_v3/REUNION_GT_SAMPLES.shp"
    
    prepare_gt(inPath, inShp, outPath)
    
    gt = "/media/je/SATA_1/Lab3/data/GT_SAMPLES.tif"
    create_time_series(inPath,gt,outPath)

    