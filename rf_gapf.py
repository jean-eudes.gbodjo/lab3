import sys
import os
import numpy as np
from sklearn.ensemble import RandomForestClassifier
from sklearn.externals import joblib
from sklearn.model_selection import GridSearchCV
from sklearn.model_selection import PredefinedSplit

train_rf = np.load(sys.argv[1])
label_train = np.load(sys.argv[2])
label_train = label_train[:,1].astype('int64')

nclasses = len(np.unique(label_train))
print (np.bincount(label_train))

valid_rf = np.load(sys.argv[3])
label_valid = np.load(sys.argv[4])
label_valid = label_valid[:,1].astype('int64')

split_id = int(sys.argv[5])
out_dir = sys.argv[6] #"rf_gapf"

sys.stdout.flush

print (train_rf.shape)
print (valid_rf.shape)


print ("RUN RANDOM FOREST")
tuned_parameters = {'n_estimators': [200,300,400,500],
                     'max_depth': [20,40,60,80,100]}

merged_ts = np.concatenate((train_rf,valid_rf),axis=0)
merged_labels = np.concatenate((label_train,label_valid),axis=0)

mytestfold = []
mytestfold.extend([-1]*label_train.size)
mytestfold.extend([0]*label_valid.size)
ps = PredefinedSplit(test_fold=mytestfold)

clf = GridSearchCV(estimator=RandomForestClassifier(), param_grid=tuned_parameters,cv=ps,n_jobs=-1,verbose=2)
clf.fit(merged_ts, merged_labels)


if not os.path.exists(out_dir):
        os.makedirs(out_dir)

joblib.dump(clf, out_dir+"/rf_"+str(split_id)+".pkl")
