import sys
import time
import os
import tensorflow as tf
print (tf.__version__)
import numpy as np 
from sklearn.metrics import accuracy_score
from sklearn.metrics import f1_score
from sklearn.preprocessing import LabelEncoder
from sklearn.utils import shuffle
from sklearn.metrics import accuracy_score
from sklearn.metrics import f1_score

def format_to_rnn (X, n_timetamps):
    new_X = np.reshape(X,(X.shape[0],n_timestamps,-1))
    print (new_X.shape)
    return new_X

def format_label (y, n_classes, binary=True) :
    encoder = LabelEncoder()
    y_tr = encoder.fit_transform(y)
    if binary :
        y_tr = tf.keras.utils.to_categorical(y_tr, n_classes,dtype='float32')
    print (y_tr.shape)
    return y_tr

def get_batch(array, i, batch_size):
    start_id = i*batch_size
    end_id = min((i+1) * batch_size, array.shape[0])
    batch = array[start_id:end_id]
    return batch

def rnn_model (X_seq, num_units, n_classes, dropOut, is_training_ph) :
    '''
    Building GRU Computational graph
    '''
    cell = tf.nn.rnn_cell.GRUCell(num_units)
    cell = tf.nn.rnn_cell.DropoutWrapper(cell,output_keep_prob=dropOut, state_keep_prob=dropOut)
    outputs, _ = tf.nn.static_rnn(cell, X_seq, dtype=tf.float32)
    ht = outputs[-1] # Shape (Batch_Size, num_units)
    fc1 = tf.layers.dense(ht,num_units,activation=tf.nn.relu)
    fc1 = tf.layers.dropout(fc1, rate= 1-dropOut, training=is_training_ph)
    fc2 = tf.layers.dense(fc1,num_units,activation=tf.nn.relu)
    fc2 = tf.layers.dropout(fc2, rate= 1-dropOut, training=is_training_ph)
    logits = tf.layers.dense(fc2,n_classes)
    return logits
    
def run(train_X, train_y, valid_X, valid_label, output_dir_models, split_numb, n_timestamps, 
                        n_classes, num_units, batch_size, n_epochs, learning_rate, drop) :
    n_bands = train_X.shape[2]
    X = tf.placeholder(tf.float32,shape=(None,n_timestamps,n_bands),name='X')
    y = tf.placeholder(tf.float32,shape=(None,n_classes),name='y')
    dropOut = tf.placeholder(tf.float32, shape=(), name="drop_rate")
    is_training_ph = tf.placeholder(tf.bool, shape=(), name="is_training")

    X_seq = tf.unstack(X,axis=1)
    # W = tf.Variable(tf.random_normal([num_units, n_classes]))
    # b = tf.Variable(tf.random_normal([n_classes]))
    # logits = tf.matmul(ht, W) + b

    logits = rnn_model(X_seq,num_units,n_classes,drop,is_training_ph)

    with tf.variable_scope("cost_prediction"):
        cost = tf.math.reduce_mean(tf.nn.softmax_cross_entropy_with_logits_v2(labels=y,logits=logits))
        
        prediction = tf.nn.softmax(logits)
        valid_prediction = tf.math.argmax(prediction,1,name="test_prediction")
        correct = tf.math.equal(tf.math.argmax(prediction,1),tf.argmax(y,1))
        accuracy = tf.reduce_mean(tf.dtypes.cast(correct,tf.float64))

    optimizer = tf.train.AdamOptimizer(learning_rate=learning_rate).minimize(cost)

    # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #

    n_batch = int(train_X.shape[0] / batch_size)
    if train_X.shape[0] % batch_size != 0:
        n_batch+=1
    print ("n_batch: %d" %n_batch)

    saver = tf.train.Saver()
    best_acc = sys.float_info.min

    init = tf.global_variables_initializer()
    with tf.Session() as session:
        session.run(init)

        for epoch in range(1,n_epochs+1):
            start = time.time()
            epoch_loss = 0
            epoch_acc = 0
            train_X, train_y = shuffle(train_X, train_y,random_state=8896)

            for batch in range(n_batch):
                batch_X = get_batch(train_X,batch,batch_size)
                batch_y = get_batch(train_y,batch,batch_size)

                acc, loss,_ = session.run([accuracy, cost, optimizer],feed_dict={X:batch_X,
                                                                                 y:batch_y,
                                                                                 dropOut:1-drop,
                                                                                 is_training_ph: True})
                
                epoch_loss += loss
                epoch_acc += acc

                del batch_X
                del batch_y
 
            stop = time.time()
            elapsed = stop - start
            print ("EPOCH ",epoch, " Train loss:",epoch_loss/n_batch,"| Accuracy:",epoch_acc/n_batch, "| Time: ",elapsed)
            
            # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #

            valid_batch = int(valid_X.shape[0] / (4*batch_size))
            if valid_X.shape[0] % (4*batch_size) != 0:
                valid_batch+=1

            total_pred = None
            
            for ibatch in range(valid_batch):
                valid_batch_X = get_batch(valid_X,ibatch,4*batch_size)

                batch_pred = session.run(valid_prediction,feed_dict={X:valid_batch_X,
                                                                    dropOut:1,
                                                                    is_training_ph: False})
                del valid_batch_X
                
                if total_pred is None :
                    total_pred = batch_pred
                else : 
                    total_pred = np.hstack((total_pred,batch_pred))
                # for pred in batch_pred :
                #     total_pred.append(pred)
            
            print (np.bincount(np.array(total_pred)))
            print (np.bincount(np.array(valid_label)))
            print ("PREDICTION")
            print ("TEST F-Measure: %f" % f1_score(valid_label, total_pred, average='weighted'))
            print (f1_score(valid_label, total_pred, average=None))
            print ("TEST Accuracy: %f" % accuracy_score(valid_label, total_pred))

            val_acc = accuracy_score(valid_label, total_pred)
            if val_acc > best_acc:
                save_path = saver.save(session, output_dir_models+"/model_"+str(split_numb))
                print("Model saved in path: %s" % save_path)
                best_acc = val_acc

if __name__ == '__main__':

    # Reading data
    train_ts = np.load(sys.argv[1])
    # print ("train_ts:", train_ts.shape)

    train_label = np.load(sys.argv[2])
    train_label = train_label[:,1]
    train_label = train_label.astype('int64')
    # print ("train_label:", train_label.shape)

    n_classes = len(np.unique(train_label))
    
    valid_ts = np.load(sys.argv[3])
    # print ("valid_ts:", valid_ts.shape)

    valid_label = np.load(sys.argv[4])
    valid_label = valid_label[:,1]
    valid_label = valid_label.astype('int64')
    # print ("valid_label:", valid_label.shape)

    split_numb = int(sys.argv[5])
    output_dir_models = sys.argv[6]
    n_timestamps = int(sys.argv[7]) 

    sys.stdout.flush

    # Format data and label to RNN
    train_X = format_to_rnn(train_ts, n_timestamps)
    train_y = format_label(train_label, n_classes)

    valid_X = format_to_rnn(valid_ts, n_timestamps)
    valid_y = format_label(valid_label, n_classes, binary=False)

    # Run Model
    num_units = 512
    batch_size = 128
    n_epochs = 500
    learning_rate = 0.0001 
    drop = 0.5
    run(train_X, train_y, valid_X, valid_y, output_dir_models, split_numb, n_timestamps, n_classes, num_units, 
                                                                    batch_size, n_epochs, learning_rate, drop)
