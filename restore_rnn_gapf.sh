#!/bin/bash
export CUDA_VISIBLE_DEVICES=0

dir="."
path_ts_gapf="splits/gapfTimeSeries"
path_gt="splits/ground_truth"
train_perc=50

for i in {0..9}
do
    echo $i
    python restore_rnn_gapf.py $path_ts_gapf/test_x$i\_$train_perc.npy $path_gt/test_y$i\_$train_perc.npy $i rnn_gapf 21
done