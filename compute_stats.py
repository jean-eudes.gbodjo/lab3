import numpy as np
import sys
import glob
import os
from sklearn.metrics import cohen_kappa_score
from sklearn.metrics import accuracy_score
from sklearn.metrics import f1_score


def transform(val):
	return round((float( int(val*10000) ) / 100),2)
	

gt_folder = sys.argv[1] # splits/ground_truth
result_folder = sys.argv[2] # rf_gapf
train_perc = sys.argv[3] # 50
# value_to_add = int(sys.argv[4]) # 0

fm = []
kappa = []
acc = []

vec = None
count = 0
for i in range(10): #10
	gtFName = gt_folder+"/test_y"+str(i)+"_"+str(train_perc)+".npy"
	#print (gtFName)
	clFName = result_folder+"/results_"+str(i)+".npy"
	#print (clFName)
	if os.path.exists(clFName):
		print ("LOAD")
		gt = np.load(gtFName)
		gt = gt[:,1] 
		cl = np.load(clFName) #+ value_to_add
		kappa.append( cohen_kappa_score(gt,cl) )
		fm.append( f1_score(gt,cl, average="weighted") )
		acc.append( accuracy_score(gt,cl))
		print ("%d: %f" % (i, f1_score(gt,cl, average="weighted") ))
		if vec is None:	
			vec = np.array( f1_score(gt,cl, average=None) )
		else:
			vec = vec + np.array( f1_score(gt,cl, average=None) )
		count+=1


print ("%.2f $\pm$ %.2f & %.4f $\pm$ %.4f & %.2f $\pm$ %.2f \\\\ \hline" % ( transform(np.mean(fm)), transform( np.std(fm)), round(np.mean(kappa),2) , round(np.std(kappa),2), transform( np.mean(acc) ), transform( np.std(acc) )   ))
vec = vec / count
nvec = [str( transform(el)) for el in vec]
print (' & '.join(nvec))


