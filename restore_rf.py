import sys
import os
import numpy as np
from sklearn.externals import joblib


model_folder = sys.argv[1]
test_data_folder = sys.argv[2]
run_id = sys.argv[3]
train_perc = sys.argv[4]

token = model_folder.split("/")[-1]

print ("START")

res_dir = model_folder+"/results"
if not os.path.exists(res_dir):
        os.makedirs(res_dir)

print ("DIRECTORY CREATED")

el = model_folder+"/rf_"+run_id+".pkl"
print (el)
clf = joblib.load(el)

test_data = np.load(test_data_folder+"/gapfTimeSeries/test_x"+run_id+"_"+train_perc+".npy")
print (test_data.shape)
	
print ("DATA LOADED")

rf_classif = clf.predict(test_data)
np.save(res_dir+"/results_"+str(run_id)+".npy",np.array(rf_classif))
